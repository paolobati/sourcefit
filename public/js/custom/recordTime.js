$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });

    //USER ON CHANGE
    $('select[name="user_id"]').on('change', function () {
        $('input[name="date"]').val('');
        $('input[name="time_in"]').val('');
        $('input[name="time_out"]').val('');
    });

    //SUBMIT
    $('#btn-submit-time').click(function (e) {
        e.preventDefault();
        user_id = $('select[name="user_id"]').find(":selected").val();
        schedule = $('select[name="schedule"]').find(":selected").val();
        date = $('input[name="date"]').val();
        time_in = $('input[name="time_in"]').val();
        time_out = $('input[name="time_out"]').val();
        if (user_id == '') {
            alert('Select User');
            return false;
        } else if (date == '') {
            alert('Select Date');
            return false;
        } else if (time_in == '' && time_out == '') {
            alert('Nothing to save');
            return false;
        } else if (time_in != '' && time_out != '' && (time_in > time_out)) {
            alert('Time out should not be greater than time in');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/record/time/save/details',
            dataSrc: '',
            dataType: 'json',
            data: {
                'user_id': user_id,
                'date': date,
                'schedule': schedule,
                'time_in': time_in,
                'time_out': time_out
            },
            beforeSend: function () {
                $("#loading-overlay").show();
            },
            success: function (response) {
                $("#loading-overlay").hide();
                if (response.status == 'success') {
                    alert('Time Saved');
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#loading-overlay").hide();
                console.log(jqXhr);
                console.log(textStatus);
                console.log(errorThrown);
                alert('Something went wront. Please reload the page');
            }
        });

    });

    //DATE ON CHANGE
    $('input[name="date"]').focusout(function () {
        user_id = $('select[name="user_id"]').find(":selected").val();
        date = $('input[name="date"]').val();
        if (date == '') {
            return false;
        } else if (user_id == '') {
            alert('Select User');
            return false;
        }
        date = $('input[name="date"]').val();
        $.ajax({
            type: 'POST',
            url: '/record/time/get/details',
            dataSrc: '',
            dataType: 'json',
            data: {
                'user_id': user_id,
                'date': date,
            },
            beforeSend: function () {
                $("#loading-overlay").show();
            },
            success: function (response) {
                $("#loading-overlay").hide();
                if (response.has_data) {
                    $('select[name="schedule"]').val(response.schedule);
                    $('input[name="time_in"]').val(response.time_in);
                    $('input[name="time_out"]').val(response.time_out);
                } else {
                    $('select[name="schedule"]').val(response.schedule);
                    $('input[name="time_in"]').val('');
                    $('input[name="time_out"]').val('');
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#loading-overlay").hide();
                console.log(jqXhr);
                console.log(textStatus);
                console.log(errorThrown);
                alert('Something went wront. Please reload the page');
            }
        });
    });
});
