$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });

    //daterangepicker init
    $('input[name="daterange"]').daterangepicker({
        opens: 'left'
    });

    //button submit
    $('#btn-submit-time').click(function(e){
        e.preventDefault();
        var end_date=  $('input[name="daterange"]').data('daterangepicker').endDate.format('YYYY-MM-DD');
        var start_date = $('input[name="daterange"]').data('daterangepicker').startDate.format('YYYY-MM-DD');
        user_id = $('select[name="user_id"]').find(":selected").val();

        if(user_id == ''){
            alert('Select User');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: '/logs/get',
            dataSrc: '',
            dataType: 'json',
            data: {
                'user_id': user_id,
                'start_date' : start_date,
                'end_date' : end_date,
            },
            beforeSend: function () {
                $("#loading-overlay").show();
            },
            success: function (response) {
                $("#loading-overlay").hide();
                if(response.logs_count == 0){
                    removeTable();
                } else {
                    generateTable(response.logs, response.columns);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#loading-overlay").hide();
                console.log(jqXhr);
                console.log(textStatus);
                console.log(errorThrown);
                alert('Something went wront. Please reload the page');
            }
        });

    });
});

function removeTable(){
    $("#results-table").empty();
    $("#results-table").append('<tr><td>NO RESULTS</td></tr>');
}

function generateTable(logs, columns){
    $("#results-table").empty();
    tableString = '';
    tableString += '<thead>';
    tableString += '<tr>';

    for (const [key, value] of Object.entries(columns)) {
        tableString += "<th>"+value+"</th>";
    }
    tableString += '</tr>';

    for (const [key, value] of Object.entries(logs)) {
        tableString += "<tr>";
        for (const [key, index] of Object.entries(columns)) {
            tableString += "<td>"+value[index]+"</td>";
        }
        tableString += "</tr>";
    }
    $("#results-table").append(tableString);

}
