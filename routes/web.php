<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/users');
});
Route::resource('users', 'UserController');

Route::get('/record/time','RecordTimeController@index');
Route::post('/record/time/get/details','RecordTimeController@getDetails');
Route::post('/record/time/save/details','RecordTimeController@saveDetails');

Route::get('/logs','LogController@index');
Route::post('/logs/get','LogController@getLogs');

