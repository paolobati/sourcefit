<?php

namespace App\Classes;

use App\Constants\Schedule;

class TimeHelper
{
    /**
     * @param $startTime
     * @param $endTime
     * @param $breakTime
     * @return float|int|mixed
     */
    public static function getDifferenceInMinutes($startTime, $endTime, $breakTime = 0){
        if($startTime == '' || $endTime == ''){
            return 0;
        }
        $dateTimeObject1 = date_create($startTime);
        $dateTimeObject2 = date_create($endTime);

        // Calculating the difference between DateTime objects
        $interval = date_diff($dateTimeObject1, $dateTimeObject2);

        $minutes = $interval->days * 24 * 60;
        $minutes += $interval->h * 60;
        $minutes += $interval->i;

        if($minutes <= $breakTime){
            return 0;
        }

        return $minutes - $breakTime;
    }

    /**
     * @param $schedule
     * @return string
     */
    public static function getStartOfShift($schedule){
        if($schedule >= 10){
            return $schedule.':00:00';
        } else {
            return '0'.$schedule.':00:00';
        }
    }

    /**
     * @param $schedule
     * @return string
     */
    public static function getEndOfShift($schedule){
        $end = $schedule + Schedule::HOURS_OF_SHIFT;
        if($end >= 24){
            $end -= 24;
        }

        if($end >= 10){
            return $end.':00:00';
        } else {
            return '0'.$end.':00:00';
        }
    }

    /**
     * @param $minutes
     * @return string
     */
    public static function convertMinutesToHoursString($minutes){
        $hours = $minutes / 60;
        return number_format($hours, 2). '('.$minutes.' minutes)';
    }
}
