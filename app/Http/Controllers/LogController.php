<?php

namespace App\Http\Controllers;

use App\Classes\TimeHelper;
use App\Constants\Schedule;
use App\Models\Hours;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'users' => User::all()
        ];
        return view('logs', $data);
    }


    /**
     * @param Request $request
     * @return false|string
     */
    public function getLogs(Request $request){
        $userId = $request->user_id;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $hours = Hours::where('user_id', $userId)
            ->where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->get();

        $logs = [];

        foreach($hours as $log){
            $name = $log->user->name();
            $schedule = $log->schedule;
            $startOfShift = TimeHelper::getStartOfShift($schedule);
            $endOfShift = TimeHelper::getEndOfShift($schedule);
            $overtime = 0;

            $hoursWorked = TimeHelper::getDifferenceInMinutes(
                $log->time_in,
                $log->time_out,
                Schedule::BREAK_TIME_IN_MINUTES
            );

            $diffenceOfStartOfShiftAndTimeIn = TimeHelper::getDifferenceInMinutes(
                $log->time_in,
                $startOfShift
            );
            if($startOfShift >= $log->time_in){
                $late = 0;
                $overtime += $diffenceOfStartOfShiftAndTimeIn;
            } else {
                $late = $diffenceOfStartOfShiftAndTimeIn;

            }

            $diffenceOfEndOfShiftAndTimeOut = TimeHelper::getDifferenceInMinutes(
                $log->time_out,
                $endOfShift
            );
            if($endOfShift <= $log->time_out){
                $undertime = 0;
                $overtime += $diffenceOfEndOfShiftAndTimeOut;
            } else {
                $undertime = $diffenceOfEndOfShiftAndTimeOut;
            }

            $logs [] = [
                'Name' => $name,
                'Date' => $log->date,
                'Schedule' => Schedule::TYPES[$log->schedule],
                'Time In' => $log->time_in,
                'Time Out' => $log->time_out,
                'Hours Worked' => TimeHelper::convertMinutesToHoursString($hoursWorked),
                'Hours Late' => TimeHelper::convertMinutesToHoursString($late),
                'Hours Undertime' => TimeHelper::convertMinutesToHoursString($undertime),
                'Hours Overtime' => TimeHelper::convertMinutesToHoursString($overtime)
            ];
        }
        $columns = [
            'Name',
            'Date',
            'Schedule',
            'Time In',
            'Time Out',
            'Hours Worked',
            'Hours Late',
            'Hours Undertime',
            'Hours Overtime'
        ];
        return json_encode([
            'logs' => $logs,
            'columns' => $columns,
            'logs_count' => count($logs)
        ]);
    }
}
