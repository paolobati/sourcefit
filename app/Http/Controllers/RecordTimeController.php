<?php

namespace App\Http\Controllers;

use App\Constants\Schedule;
use App\Models\Hours;
use App\Models\User;
use Illuminate\Http\Request;

class RecordTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'users' => User::all(),
            'scheduleTypes' => Schedule::TYPES
        ];
        return view('recordTime', $data);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function getDetails(Request $request){
        $hours = Hours::where([
            'user_id' => $request->user_id,
            'date' => $request->date
        ])->first();
        if(!$hours) {
            $user = User::find($request->user_id);
            return json_encode([
                'has_data' => false,
                'time_in' => false,
                'time_out' => false,
                'schedule' => $user->schedule,
            ]);
        } else {
            return json_encode([
                'has_data' => true,
                'time_in' => $hours->time_in,
                'time_out' => $hours->time_out,
                'schedule' => $hours->schedule,
            ]);
        }
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function saveDetails(Request  $request){
        $hours = Hours::firstOrNew([
            'user_id' => $request->user_id,
            'date' => $request->date
        ]);
        $hours->schedule = $request->schedule;
        $hours->time_in = $request->time_in;
        $hours->time_out = $request->time_out;
        $hours->save();
        return json_encode([
            'status' => 'success'
        ]);
    }
}
