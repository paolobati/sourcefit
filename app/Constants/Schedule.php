<?php

namespace App\Constants;

class Schedule {
    //Index = Time_in(military Time)
    const TYPES = [
        '8' => '8am - 5pm',
        '9' => '9am - 6pm',
        '10' => '10am - 7pm',
    ];

    const HOURS_OF_SHIFT = 9;
    const BREAK_TIME_IN_MINUTES = 60;
}
