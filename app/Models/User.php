<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'schedule',
        'contact'
    ];
    protected $table = 'users';

    /**
     * @return array
     */
    public static function getFields(){
        return [
            ['name' => 'first_name', 'label' => 'First Name', 'type' => 'text', 'required' => 1],
            ['name' => 'middle_name', 'label' => 'Middle Name', 'type' => 'text'],
            ['name' => 'last_name', 'label' => 'Last Name', 'type' => 'text'],
            ['name' => 'contact', 'label' => 'Contact', 'type' => 'text'],
            ['name' => 'email', 'label' => 'Email', 'type' => 'email', 'required' => 1],
            ['name' => 'schedule', 'label' => 'Schedule', 'type' => ''],
        ];
    }

    /**
     * @return mixed|string
     */
    public function name(){
        $fullName = trim($this->first_name);
        if($this->middle_name != ''){
            $fullName .= ' '.trim($this->middle_name);
        }
        if($this->last_name != ''){
            $fullName .= ' '.trim($this->last_name);
        }
        return $fullName;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hours()
    {
        return $this->hasMany(Hours::class);
    }
}
