<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAndEmployeeHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('first_name');
                $table->string('last_name')->nullable();
                $table->string('middle_name')->nullable();
                $table->string('contact')->nullable();
                $table->string('email');
                $table->string('schedule');
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('hours')) {
            Schema::create('hours', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->date('date');
                $table->time('time_in');
                $table->time('time_out');
                $table->string('schedule');
                $table->unsignedBigInteger('user_id')->nullable();
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hours');
        Schema::dropIfExists('users');
    }
}
