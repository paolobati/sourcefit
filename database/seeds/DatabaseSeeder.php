<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         //ADD 10 USERS
        for($i = 0; $i < 10; $i++){
            \Illuminate\Support\Facades\DB::table('users')->insert([
                'first_name' => 'F_'.\Illuminate\Support\Str::random(5),
                'middle_name' => 'M_'.\Illuminate\Support\Str::random(5),
                'last_name' => 'L_'.\Illuminate\Support\Str::random(5),
                'schedule' => rand(8, 10),
                'contact' => rand(8800000,8899999),
                'email' => \Illuminate\Support\Str::random(10).'@gmail.com'
            ]);
        }



        for($i = 0; $i < 100; $i++){
            \Illuminate\Support\Facades\DB::table('hours')->insert([
                'date' => $this->generateRandomDate(),
                'time_in' => $this->generateTimeIn(),
                'time_out' => $this->generateTimeOut(),
                'schedule' => rand(8, 10),
                'user_id' => rand(1, 10)
            ]);
        }
    }

    /**
     * @return string
     */
    private function generateTimeIn(){
        return str_pad(rand(0,10), 2, "0", STR_PAD_LEFT).
            ":".str_pad(rand(0,59), 2, "0", STR_PAD_LEFT).
            ":00";
    }

    /**
     * @return string
     */
    private function generateTimeOut(){
        return str_pad(rand(12,23), 2, "0", STR_PAD_LEFT).
            ":".str_pad(rand(0,59), 2, "0", STR_PAD_LEFT).
            ":00";
    }

    /**
     * @return DateTime
     * @throws Exception
     */
    private function generateRandomDate($format = 'Y-m-d'){
        $start_date = '2022-01-01';
        $end_date = date('Y-m-d');

        $min = strtotime($start_date);
        $max = strtotime($end_date);

        // Generate random number using above bounds
        $val = rand($min, $max);

        // Convert back to desired date format
        return new DateTime(date('Y-m-d', $val));
    }

}
