<nav class="navbar navbar-expand bg-light">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        User
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="/users/create">Add</a></li>
                        <li><a class="dropdown-item" href="/users">View All</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/record/time">Record Time</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logs">Logs</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
