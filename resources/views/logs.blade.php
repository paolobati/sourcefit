<!-- index.blade.php -->
@extends('layout.index')

@section('content')
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif
    <form id="record-form">
        <meta name="_token" content="{{ csrf_token() }}">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td>User</td>
                <td>
                    <select class="form-control" name="user_id">
                        <option value="" disabled selected> -</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name()}}</option>
                        @endforeach
                    </select>
                </td>
            <tr>
            <tr>
                <td>Date Range</td>
                <td>
                    <input type="text" name="daterange" class="form-control" value="" />
                </td>
            <tr>
                <td colspan="2">
                    <button class="btn btn-danger" id="btn-submit-time" type="button">View</button>
                </td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered" id="results-table">

        </table>
    </form>
@endsection
@push('add-script')
    <script src="/js/custom/logs.js"></script>
@endpush
