<!-- index.blade.php -->
@extends('layout.index')

@section('content')
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif
    <table class="table table-striped">
        <thead>
        <tr>
            <td>Name</td>
            <td>Schedule</td>
            <td>Email</td>
            <td>Contact</td>
            <td></td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->name()}}</td>
                <td>
                    @if(isset($scheduleTypes[$user->schedule]))
                        {{$scheduleTypes[$user->schedule]}}
                    @endif
                </td>
                <td>{{$user->email}}</td>
                <td>{{$user->contact}}</td>
                <td><a href="{{ route('users.edit', $user->id)}}" class="btn btn-primary">Edit</a></td>
                <td>
                    <form action="{{ route('users.destroy', $user->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
