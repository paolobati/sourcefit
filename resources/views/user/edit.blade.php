<!-- update.blade.php -->
@extends('layout.index')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
        .form-group{
            padding: 10px;
        }
        .form-group.required label:after {
            content:"*";
            color:red;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Edit User Form
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <form method="post" action="{{ route('users.update', $user->id ) }}">
                @csrf
                @method('PATCH')
                <div class="row">
                    @foreach($fields as $field)
                        <div class="form-group col-md-6 @if(isset($field['required'])) required @endif">
                            <label for="{{$field['name']}}">{{$field['label']}}</label>
                                <?php $fieldName = $field['name']; ?>
                            @if($fieldName == 'schedule')
                                <select class="form-control" name="{{$fieldName}}">
                                    @foreach($scheduleTypes as $typeIndex => $type)
                                        <option value="{{$typeIndex}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            @else
                                <input type="{{$field['type']}}" class="form-control" name="{{$field['name']}}" value="{{$user->$fieldName}}"/>
                            @endif

                        </div>
                    @endforeach

                    <div class="form-group col-12">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
