<!-- index.blade.php -->
@extends('layout.index')

@section('content')
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br/>
    @endif
    <form id="record-form">
        <meta name="_token" content="{{ csrf_token() }}">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td>User</td>
                <td>
                    <select class="form-control" name="user_id">
                        <option value="" disabled selected> -</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name()}}</option>
                        @endforeach
                    </select>
                </td>
            <tr>
                <td>Date</td>
                <td><input name="date" type="date" class="form-control"></td>
            </tr>
            <tr>
                <td>Schedule</td>
                <td>
                    <select class="form-control" name="schedule">
                        @foreach($scheduleTypes as $typeIndex => $type)
                            <option value="{{$typeIndex}}">{{$type}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>Time In</td>
                <td><input type="time" name="time_in" class="form-control"></td>
            </tr>
            <tr>
                <td>Time Out</td>
                <td><input type="time" name="time_out" class="form-control"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <button class="btn btn-danger" id="btn-submit-time" type="button">Save</button>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
@endsection
@push('add-script')
    <script src="/js/custom/recordTime.js"></script>
@endpush
