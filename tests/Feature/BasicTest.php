<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BasicTest extends TestCase
{
    public function testExample()
    {
        $response = $this->get('/users');
        $response->assertStatus(200);
        $response->assertSee('users');

    }
}
